require('./config');
const Mongoose = require('mongoose').Mongoose;

const dbImplenet = new Mongoose();
const dbCMS = new Mongoose();

dbImplenet.connect(process.env.urlMongo, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    autoReconnect: true,
    reconnectTries: 10000,
    reconnectInterval: 1000
}).then(resp => {
    console.log('--------------------------------')
    console.log('CONEXION A MONGO IMP ESTABLECIDA ');
})


dbCMS.connect(process.env.urlMongocms, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    autoReconnect: true,
    reconnectTries: 10000,
    reconnectInterval: 1000
}).then(resp => {
    console.log('--------------------------------')
    console.log('CONEXION A MONGO CMS ESTABLECIDA ');
})

module.exports = { dbImplenet, dbCMS };
// Lee archivo .env
const dotenv = require('dotenv');
const result = dotenv.config();

// validacion para evitar cargar todo si no esta creado el archivo .env
if (typeof process.env.AMBIENTE == "undefined") {
    console.log('Falta definir el ambiente en el archivo .env');
    console.log('renombre el archivo .env.default a .env');
    return
}

// Archivo para definir las variables de entorno que se ocuparan en todo el sitio web
if (process.env.AMBIENTE && process.env.AMBIENTE.trim() == 'pro') {

    console.log('PRODUCCION');
    process.env.urlMongo = `mongodb://ImplenetApi:3NSIkjCW5SZ4XnKXBz98@10.158.0.8:27017/Implenet`;
    process.env.urlMongocms = `mongodb://ImplenetApi:3NSIkjCW5SZ4XnKXBz98@10.158.0.8:27017/b2b-imp`;
    process.env.urlApiElastic = `http://35.247.221.172:4200/api`;
    process.env.urlWebApiCarro = `http://replicacion.implementos.cl/apicliente/api`;
    process.env.urlWebApiDespacho = `http://replicacion.implementos.cl/apilogistica/api`;
    process.env.urlWebApiCliente = `http://replicacion.implementos.cl/apiCliente/api`;
    // process.env.urlMongocms = `mongodb://root:yW9LPLBxAEmm@10.158.0.8:27017/b2b-imp`;
    process.env.urlApiElastic = `http://35.247.221.172:4200/api`;

} else {

    console.log('DESARROLLO');
    process.env.urlMongo = `mongodb://192.168.211.77:27017/Implenet`;
    process.env.urlMongocms = `mongodb://192.168.211.77:27017/b2b-imp`;
    process.env.urlApiElastic = `http://35.247.221.172:4200/api`;
    process.env.urlWebApiCarro = `http://localhost:50475/api`;
    process.env.urlWebApiDespacho = `http://localhost:50595/api`;
    process.env.urlWebApiCliente = `http://localhost:50340/api`;
}
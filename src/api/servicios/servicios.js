const axios = require('axios');
require('../../config/config');


const getBloqueo = async(rut) => {

    console.log(3);
    const encodedUlr = encodeURI(rut);
    console.log(process.env.urlWebApiCliente);
    console.log(rut);
    const instance = axios.create({
        baseURL: `${process.env.urlWebApiCliente}/cliente/bloqueo?rut=${rut}`,

    });

    console.log(instance);
    const resp = await instance.get();
    if (resp.data.Results === 0) {
        throw new Error(`Ocurrio un error al buscar bloqueo de ${ rut }`);
    }
    const data = resp.data;
    return data;
}

const getSaldo = async(rut) => {

    const encodedUlr = encodeURI(rut);
    console.log(process.env.urlWebApiCliente);
    const instance = axios.create({
        baseURL: `${process.env.urlWebApiCliente}/cliente/saldo?rut=${encodedUlr}`
    });

    const resp = await instance.get();
    if (resp.data.Results === 0) {
        throw new Error(`Ocurrio un error al buscar saldo de ${ rencodedUlrut }`);
    }
    const data = resp.data;
    return data;
}

const getFacturas = async(rut) => {

    const encodedUlr = encodeURI(rut);
    console.log(process.env.urlWebApiCliente);
    const instance = axios.create({
        baseURL: `${process.env.urlWebApiCliente}/cliente/facturas?rut=${encodedUlr}`
    });

    const resp = await instance.get();
    if (resp.data.Results === 0) {
        throw new Error(`Ocurrio un error al buscar facturas pendientes ${ encodedUlr }`);
    }
    const data = resp.data;
    return data;
}

const getVentas = async(rut, cantidad) => {
    const encodedUlr = encodeURI(rut);
    console.log(process.env.urlWebApiCliente);
    const instance = axios.create({
        baseURL: `${process.env.urlWebApiCliente}/cliente/ventas?rut=${encodedUlr}&cantidad=${cantidad}`
    });

    const resp = await instance.get();
    if (resp.data.Results === 0) {
        throw new Error(`Ocurrio un error al buscar ventas ${ encodedUlr }`);
    }
    const data = resp.data;
    return data;
}

const setContacto = async(contacto) => {

    const resp = await axios.post(`${process.env.urlWebApiCliente}/cliente/contacto`, JSON.stringify(contacto), {
        headers: { 'Content-Type': 'application/json' }
    });
    if (resp.data.Results === 0) {
        throw new Error(`Ocurrio un error al crear el contacto`);
    }
    const data = resp.data;
    return data;
}

const setDireccion = async(direccion) => {

    const resp = await axios.post(`${process.env.urlWebApiCliente}/cliente/direccion`, JSON.stringify(direccion), {
        headers: { 'Content-Type': 'application/json' }
    });
    if (resp.data.Results === 0) {
        throw new Error(`Ocurrio un error al crear la direccion`);
    }
    const data = resp.data;
    return data;
}

const obtenerClientes = async ()=>{
    
    const resp = await axios.get(`http://replicacion.implementos.cl/wsOmnichannel/Capa_jsonServices/getCOVendedor.ashx?rutVendedor=15749518-6&rutCliente&nombreCliente`,{
        headers: { 'Content-Type': 'application/json' }
    });
    if (resp.data.Results === 0) {
        throw new Error(`Ocurrio un error al obtener listado de clientes`);
    }
    const data = resp.data;
    return data;
}

const formaPagoCliente = async (rutCliente)=>{
 
    const url = `http://replicacion.implementos.cl/wsOmnichannel/Capa_jsonServices/getTipoFormaPago.ashx?rutCliente=${rutCliente}`;
    const resp = await axios.get(url,{
        headers: { 'Content-Type': 'application/json' }
    });
    if (resp.data.Results === 0) {
        throw new Error(`Ocurrio un error al obtener formas de pago del cliente`);
    }
    const data = resp.data;
    return data;
}

const eventoCli = async (parametros)=>{

    const endPoint = `http://replicacion.implementos.cl/wsOmnichannel/Capa_jsonServices/setCotizacion.ashx`;
    const params = `?rutEmisor=${parametros.rutEmisor}&rutVendedor=${parametros.rutVendedor}&rutCliente=${parametros.rutCliente}&codTipoFormaPago=${parametros.codTipoFormaPago}&codOC&detalle=${parametros.detalle}&codSucursal=${parametros.codSucursal}&idVisita=55482`;
    const url =`${endPoint}${params}`
    const resp = await axios.get(url,{
        headers: { 'Content-Type': 'application/json' }
    });
    if (resp.data.Results === 0) {
        throw new Error(`Ocurrio un error al obtener formas de pago del cliente`);
    }
   
    const data = resp.data;
    return data;
}

const checkInCliente = async (parametros)=>{
    
    const endPoint = `http://replicacion.implementos.cl/wsOmnichannel/Capa_jsonServices/setEvento.ashx`;
    const params = `?idEvento&refVisita&tipoEvento=VISITA&fechaCreacion=${parametros.fechaCreacion}&fechaEjecucion&fechaProgramada=${parametros.fechaProgramada}&observaciones=${parametros.observaciones}&rutEmisor=${parametros.rutEmisor}&rutVendedor=${parametros.rutVendedor}&rutCliente=${parametros.rutCliente}&estado=PROGRAMADA&direccion=${parametros.direccion}&latitud=${parametros.latitud}&longitud=${parametros.longitud}&referenciaEvento`;
    const url =`${endPoint}${params}`
    
    const resp = await axios.get(url,{
        headers: { 'Content-Type': 'application/json' }
    });
  
    if (resp.data.Results === 0) {
        throw new Error(`Ocurrio un error al obtener formas de pago del cliente`);
    }
   
    const data = resp.data;
    return data;
}

const stockArticulo = async (parametros)=>{
    
    const url =`http://replicacion.implementos.cl/WebApiBuscador2/api/buscador/stock?sku=${parametros.sku}&sucursal=${parametros.sucursal}&usuario=72`
    const resp = await axios.get(url,{
        headers: { 'Content-Type': 'application/json' }
    });
    if (resp.data.Results === 0) {
        throw new Error(`Ocurrio un error al obtener formas de pago del cliente`);
    }
   
    const data = resp.data;
    return data;
}

module.exports = { getBloqueo, getSaldo, getFacturas, getVentas, setContacto, setDireccion,obtenerClientes,formaPagoCliente,eventoCli,stockArticulo,checkInCliente }
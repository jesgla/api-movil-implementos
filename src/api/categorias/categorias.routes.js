
const express = require("express");
const Articulo = require("../../models/articulo")
const Categoria = require("../../models/categoria")


const app = express();

async function creaEstructura(req, res, next) {
    try {
        await Categoria.collection.drop();
        const uens = await Articulo
            .distinct('uen');

        const menu = [];

        i = 0;
        for (const uen of uens) {
            const categorias = await Articulo.aggregate([
                { $match: { uen: uen } },
                { $group: { _id: "$categoria", cant: { $sum: 1 }, id: { $first: "$id_categoria" } } },
                { $project: { nombre: "$_id", cant: 1, id: 1, _id: 0 } },
            ]);

            const categoriaLinea = [];
            for (const categoria of categorias) {
                console.log("_____________________________________")
                console.log(categoria)
                // const lineas = await Articulo.distinct('linea', { categoria: categoria.nombre });

                const lineaFull = await Articulo.aggregate([
                    { $match: { categoria: categoria.nombre } },
                    { $group: { _id: "$linea", count: { $sum: 1 }, id: { $first: "$id_linea" } } },
                    { $project: { nombre: "$_id", count: 1, id: 1, _id: 0 } }
                ]);
                console.log(lineaFull)
                categoriaLinea.push({ "nombre": categoria.nombre, "categoriaId": categoria.id, "count": categoria.cant, "lineas": lineaFull });
            }

            let itemMenu = {
                "uen": uen,
                "categorias": categoriaLinea
            }
            menu.push(itemMenu)
        }

        await Categoria.create(menu).then(result => { console.log('created') });
        res.send(menu);


    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error al obtener la ficha del producto",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};


async function obtieneCategorias(req, res, next) {
    try {
        const categorias = await Categoria.find();
        res.send(categorias);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error al obtener la ficha del producto",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};


// GET
app.get("/api/categorias/listado", obtieneCategorias);
app.get("/api/categorias/creaEstructura", creaEstructura);

module.exports = app;

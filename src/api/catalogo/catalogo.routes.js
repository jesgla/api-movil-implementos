const Articulo = require("../../models/articulo");
const fetch = require('node-fetch');
const express = require("express");
const { establecerPrecio, buscarProductosGenericos, buscarPorSku, prueba, buscarPorCategoria } = require('../catalogo/catalogo.functions');
const app = express();


// Simula productos destacados
async function productosDestacados(req, res) {
    try {
        let respuesta = await buscarProductosGenericos(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error al obtener los productos",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};

async function busquedaCategoria(req, res) {
    try {

        var url = req.url.split('/');
        var categoria;
        if (url[url.length - 1] == '') {
            categoria = url[url.length - 2];
            console.log('aaaa');
        } else {
            categoria = url[url.length - 1];
            console.log('bbb');
        }
        console.log(categoria);
        res.send(categoria);


        let respuesta = await prueba(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error al obtener los productos",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};

// Permite realiza la busqueda de productos en elastic y despues agrega precios y realiza la paginación
async function busquedaProductos(req, res) {
    try {
        const { descripcion, sucursal, rut, categoria, pagina, productosPorPagina } = req.body;

        limite = parseInt(productosPorPagina) || 16;
        const Pagina = pagina || 1;
        const skip = Pagina == 1 ? 0 : (Pagina - 1) * limite;

        if (descripcion != '' && descripcion !== undefined) {

            const descripcionSinPrefijo = descripcion
            let url = `${process.env.urlApiElastic}/articulo?word=${descripcionSinPrefijo}`;

            let response = await fetch(url);
            let respuestaElastic = await response.json();


            if (respuestaElastic.articulos.length != 0) {
                let sku = respuestaElastic.articulos.map(item => { return item.sku; })
                const data = await buscarPorSku(sku, sucursal, rut, limite, skip, Pagina, categoria);
                res.send(data);

            } else {
                let respuesta = {
                    error: false,
                    msg: "No se encontraron articulos para la busqueda solicitada!!",
                    errorDetalle: ""
                }
                res.send(respuesta);
            }
        } else {
            const data = await buscarPorCategoria(categoria, sucursal, rut, limite, skip, Pagina);
            
            res.send(data);
        }


    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error al obtener la ficha del producto",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};


// Permite cargar los datos de la ficha de productos
async function fichaProducto(req, res) {

    try {
        const articulo = await Articulo.findOne(req.params)
        const precioArticulo = establecerPrecio(articulo.toObject(), 'SAN BRNRDO');

        let respuesta = {
            error: false,
            msg: "",
            data: precioArticulo
        }
        res.send(respuesta);

    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error al obtener la ficha del producto",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }

};

// RUTAS

// GET
app.get("/api/catalogo/ficha/:sku", fichaProducto);
app.get("/api/catalogo/productosDestacados", productosDestacados);
app.get("/api/catalogo/productosRelacionados", productosDestacados);
app.get("/api/catalogo/productosMasVendidos", productosDestacados);
app.get("/api/catalogo/nuevosProductos", productosDestacados);
app.get("/api/catalogo/productosOfertasEspeciales", productosDestacados);
app.get("/api/catalogo/productosMasValorados", productosDestacados);
app.get("/api/catalogo/ultimoProductos", productosDestacados);
app.get("/api/catalogo/categoria/*", busquedaCategoria);

// POST
app.post("/api/catalogo/busquedaProductos", busquedaProductos);

module.exports = app;
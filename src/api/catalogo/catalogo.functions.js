const Articulo = require("../../models/articulo");
const Producto = require("../../models/productocms");
const CategoriaProducto = require("../../models/categoriaProductoCms");
const CategoriaCms = require("../../models/categoriacms");
const Filter = require("../../models/filtrocms");
const utils = require("../../utilities/util")
const webAPI = require('../servicios/servicios');




async function prueba(req, limit = 30) {
    const productos = await Categoria.find({}).limit(30)
    let respuesta = {
        error: false,
        msg: "",
        data: productos
    }
    return respuesta;
}

function establecerPrecio(articulo, sucursal, rut = null) {
    if (!articulo || !Array.isArray(articulo.precios)) {
        return articulo;
    }
    const precio = (() => {
        const porSucursal = articulo.precios.filter(infoPrecio => infoPrecio.sucursal == sucursal);

        if (rut) {
            const porRut = porSucursal.find(infoPrecio => infoPrecio.rut == rut);
            return porRut || porSucursal.find(infoPrecio => infoPrecio.rut == "0");
        }
        return porSucursal.find(infoPrecio => infoPrecio.rut == "0");
    })();

    const result = {...articulo, precio };
    delete result.precios;
    return result;
}

async function buscarProductosGenericos(req, limit = 30) {
    const articulos = await Articulo
        .find({
            $and: [{
                    image_recid: { $exists: true }
                }, {
                    image_recid: { $ne: 0 }
                }

            ]
        })
        .limit(limit)
        .skip(utils.randomArticulos().skip)

    let articulosNuevos = [];
    let skus = []
    for (let articulo of articulos) {
        skus.push(articulo.sku);
        articulosNuevos.push(establecerPrecio(articulo.toObject(), 'SAN BRNRDO'));
    }

    const pro = await Producto.find({ sku: { $in: skus } }, { id: 1 });

    var cat = [];
    for (var i = 0; i < pro.length; i++) {
        cat.push(pro[i].id);
    }

    const cate = await CategoriaProducto.find({ product_id: { $in: cat } });

    var fil = [];

    for (var i = 0; i < cate.length; i++) {
        fil.push(cate[i].category_id);
    }

    const catCMS = await CategoriaCms.find({ id: { $in: fil } }, { id: 1, name: 1, slug: 1, image: 1, parent_id: 1, on_header: 1, _id: 0 }).sort({ id: -1 });

    var arreglocat = [];

    for (var i = 0; i < catCMS.length; i++) {

        var obj = {
            id: catCMS[i].id,
            title: catCMS[i].name,
            url: catCMS[i].slug,
            image: catCMS[i].image,
            parent_id: catCMS[i].parent_id,
            children: []
        }
        arreglocat.push(obj);
    }

    for (let value of arreglocat) {
        if (value.parent_id != null) {
            var catcmsx = arreglocat.find(x => x.id == value.parent_id);
            if (catcmsx !== undefined)
                catcmsx.children.push(value);
        }
    }

    var arregloNivel = [];

    for (let value of arreglocat) {
        if (value.parent_id == null) {
            arregloNivel.push(value);
        }
    }

    const filt = await Filter.find({ category_id: { $in: fil } });


    let respuesta = {
        error: false,
        msg: "",
        data: {
            items: articulosNuevos,
            filter: filt,
            category: arregloNivel
        }


    }
    return respuesta;
}

async function buscarPorSku(sku, sucursal, rut = null, limit = 16, skip = 0, pagina = 0, categoria = '') {



    if (categoria != '') {

        sku = await obtieneSKUconCat(categoria, sku);
    }

    if (sku.length != 0) {


        const filtro = [{
            $match: { sku: { $in: sku } }
        }];
        const proyeccion = {
            $project: {
                sku: 1,
                atributos: 1,
                categoria: 1,
                cod_id_proveedor: 1,
                costo_cero: 1,
                costo_financiero: 1,
                estado: 1,
                fabricante: 1,
                id_categoria: 1,
                id_linea: 1,
                id_marca: 1,
                linea: 1,
                marca: 1,
                nombre: 1,
                numero_parte: 1,
                p_minimo: 1,
                product: 1,
                uen: 1,
                unit_id: 1,
                fullText: 1,
                image_recid: 1,
                descripcion: 1,
                matriz: 1,
                sku_matriz: 1,
                precios: 1,
                precio_escala: 1
            }
        };
        // ITEMS PAGINADO
        const articulos = await Articulo.aggregate([...filtro, proyeccion, { $skip: skip }, { $limit: limit }]);
        const ArticulosPrecio = articulos.map(a => establecerPrecio(a, sucursal, rut));

        // TOTAL DE ITEMS
        const totalRegistrosData = await Articulo.aggregate([...filtro, { $count: "sku" }]);

        const mostrandoHasta = (pagina * limit);
        const mostrandoDesde = (mostrandoHasta - limit) + 1;
        const totalRegistros = totalRegistrosData[0].sku;
        const totalPag = totalRegistros / limit;

        const resultadoPrecioMinimo = await Articulo.aggregate([...filtro,
            { $project: { "precios": 1 } },
            { $unwind: "$precios" },
            { $match: { "precios.sucursal": sucursal } },
            { $sort: { "precios.precio": 1 } },
            { $limit: 1 },
            { $project: { _id: false, precioMinimo: "$precios.precio" } }
        ]);

        const resultadoPrecioMaximo = await Articulo.aggregate([...filtro,
            { $project: { "precios": 1 } },
            { $unwind: "$precios" },
            { $match: { "precios.sucursal": sucursal } },
            { $sort: { "precios.precio": -1 } },
            { $limit: 1 },
            { $sort: { "precios.precio": -1 } },
            { $limit: 1 },
            { $project: { _id: false, precioMaximo: "$precios.precio" } },
        ]);

        const categoriaResultado = await Articulo.aggregate([...filtro,
            { $group: { _id: "$categoria", cant: { $sum: 1 }, id: { $first: "$id_categoria" } } },
            { $match: { _id: { $ne: null } } },
            { $project: { label: "$_id", "count": "$cant", _id: 0, type: 'child', id: 1 } }
        ]);

        const marcasResultado = await Articulo.aggregate([...filtro,
            { $group: { _id: "$marca", cant: { $sum: 1 } } },
            { $match: { _id: { $ne: null } } },
            { $project: { label: "$_id", "count": "$cant", _id: 0 } }
        ]);

        //cambiar a funcion luego 
        const pro = await Producto.find({ sku: { $in: sku } }, { id: 1 });

        var cat = [];
        for (var i = 0; i < pro.length; i++) {
            cat.push(pro[i].id);
        }

        const cate = await CategoriaProducto.find({ product_id: { $in: cat } });

        var fil = [];

        for (var i = 0; i < cate.length; i++) {
            fil.push(cate[i].category_id);
        }

        const catCMS = await CategoriaCms.find({ id: { $in: fil } }, { id: 1, name: 1, slug: 1, image: 1, parent_id: 1, on_header: 1, _id: 0 }).sort({ id: -1 });

        var arreglocat = [];

        for (var i = 0; i < catCMS.length; i++) {

            var obj = {
                id: catCMS[i].id,
                title: catCMS[i].name,
                url: catCMS[i].slug,
                image: catCMS[i].image,
                parent_id: catCMS[i].parent_id,
                children: []
            }
            arreglocat.push(obj);
        }

        for (let value of arreglocat) {
            if (value.parent_id != null) {
                var catcmsx = arreglocat.find(x => x.id == value.parent_id);
                catcmsx.children.push(value);
            }
        }

        var arregloNivel = [];

        for (let value of arreglocat) {
            if (value.parent_id == null) {
                arregloNivel.push(value);
            }
        }

        const filt = await Filter.find({ category_id: { $in: fil } });
        

        const resultado = {
            items: ArticulosPrecio,
            totalRegistros: totalRegistros,
            paginaActual: pagina,
            totalPaginas: Math.ceil(totalPag),
            mostrandoDesde: mostrandoDesde,
            mostrandoHasta: (limit > totalRegistros) ? totalRegistros : mostrandoHasta,
            filter: filt,
            category: arregloNivel

        }
        resultado.items = await AgregarStock(resultado.items);
        return resultado;
    } else {
        let respuesta = {
            error: false,
            msg: "No se encontraron productos para esta Categoria!!",
            errorDetalle: ""
        }
        return respuesta;
    }

};
AgregarStock =async (Articulos)=>{

    for (let index = 0; index < Articulos.length; index++) {
        let articulo = Articulos[index];
      
        let parametros ={
            sku : articulo.sku,
            sucursal : "CONCEPCION"
        }
        
        let stock = await webAPI.stockArticulo(parametros);
        articulo.stockorden = Number(stock.tienda);
    }
    
    return await Articulos;
}
async function buscarPorCategoria(cat, sucursal, rut = null, limit = 16, skip = 0, pagina = 0) {


    let sku = await obtieneSKU(cat);


    if (sku.length != 0) {



        const filtro = [{
            $match: { sku: { $in: sku } }
        }];
        const proyeccion = {
            $project: {
                sku: 1,
                atributos: 1,
                categoria: 1,
                cod_id_proveedor: 1,
                costo_cero: 1,
                costo_financiero: 1,
                estado: 1,
                fabricante: 1,
                id_categoria: 1,
                id_linea: 1,
                id_marca: 1,
                linea: 1,
                marca: 1,
                nombre: 1,
                numero_parte: 1,
                p_minimo: 1,
                product: 1,
                uen: 1,
                unit_id: 1,
                fullText: 1,
                image_recid: 1,
                descripcion: 1,
                matriz: 1,
                sku_matriz: 1,
                precios: 1,
                precio_escala: 1
            }
        };
        // ITEMS PAGINADO
        const articulos = await Articulo.aggregate([...filtro, proyeccion, { $skip: skip }, { $limit: limit }]);
        const ArticulosPrecio = articulos.map(a => establecerPrecio(a, sucursal, rut));

        // TOTAL DE ITEMS
        const totalRegistrosData = await Articulo.aggregate([...filtro, { $count: "sku" }]);

        const mostrandoHasta = (pagina * limit);
        const mostrandoDesde = (mostrandoHasta - limit) + 1;
        const totalRegistros = totalRegistrosData[0].sku;
        const totalPag = totalRegistros / limit;

        const resultadoPrecioMinimo = await Articulo.aggregate([...filtro,
            { $project: { "precios": 1 } },
            { $unwind: "$precios" },
            { $match: { "precios.sucursal": sucursal } },
            { $sort: { "precios.precio": 1 } },
            { $limit: 1 },
            { $project: { _id: false, precioMinimo: "$precios.precio" } }
        ]);

        const resultadoPrecioMaximo = await Articulo.aggregate([...filtro,
            { $project: { "precios": 1 } },
            { $unwind: "$precios" },
            { $match: { "precios.sucursal": sucursal } },
            { $sort: { "precios.precio": -1 } },
            { $limit: 1 },
            { $sort: { "precios.precio": -1 } },
            { $limit: 1 },
            { $project: { _id: false, precioMaximo: "$precios.precio" } },
        ]);

        const categoriaResultado = await Articulo.aggregate([...filtro,
            { $group: { _id: "$categoria", cant: { $sum: 1 }, id: { $first: "$id_categoria" } } },
            { $match: { _id: { $ne: null } } },
            { $project: { label: "$_id", "count": "$cant", _id: 0, type: 'child', id: 1 } }
        ]);

        const marcasResultado = await Articulo.aggregate([...filtro,
            { $group: { _id: "$marca", cant: { $sum: 1 } } },
            { $match: { _id: { $ne: null } } },
            { $project: { label: "$_id", "count": "$cant", _id: 0 } }
        ]);

        //cambiar a funcion luego 
        const pro = await Producto.find({ sku: { $in: sku } }, { id: 1 });

        var cat = [];
        for (var i = 0; i < pro.length; i++) {
            cat.push(pro[i].id);
        }

        const cate = await CategoriaProducto.find({ product_id: { $in: cat } });

        var fil = [];

        for (var i = 0; i < cate.length; i++) {
            fil.push(cate[i].category_id);
        }

        const catCMS = await CategoriaCms.find({ id: { $in: fil } }, { id: 1, name: 1, slug: 1, image: 1, parent_id: 1, on_header: 1, _id: 0 }).sort({ id: -1 });

        var arreglocat = [];

        for (var i = 0; i < catCMS.length; i++) {

            var obj = {
                id: catCMS[i].id,
                title: catCMS[i].name,
                url: catCMS[i].slug,
                image: catCMS[i].image,
                parent_id: catCMS[i].parent_id,
                children: []
            }
            arreglocat.push(obj);
        }

        for (let value of arreglocat) {
            if (value.parent_id != null) {
                var catcmsx = arreglocat.find(x => x.id == value.parent_id);
                catcmsx.children.push(value);
            }
        }

        var arregloNivel = [];

        for (let value of arreglocat) {
            if (value.parent_id == null) {
                arregloNivel.push(value);
            }
        }

        const filt = await Filter.find({ category_id: { $in: fil } });


        //termino de funcion 


        const resultado = {
            items: ArticulosPrecio,
            totalRegistros: totalRegistros,
            paginaActual: pagina,
            totalPaginas: Math.ceil(totalPag),
            mostrandoDesde: mostrandoDesde,
            mostrandoHasta: (limit > totalRegistros) ? totalRegistros : mostrandoHasta,
            filter: filt,
            category: arregloNivel

        }



        return resultado;
    } else {
        let respuesta = {
            error: false,
            msg: "No se encontraron productos para esta Categoria!!",
            errorDetalle: ""
        }
        return respuesta;
    }
}

async function obtieneSKU(cat) {
    let sku = [];
    var idCategoria = await CategoriaCms.findOne({ slug: cat }, { id: 1, _id: 0 });
    if (idCategoria != null && idCategoria.length != 0) {
        var idProductos = await CategoriaProducto.find({ category_id: idCategoria.id }, { product_id: 1, _id: 0 });

        if (idProductos != null && idProductos.length != 0) {

            var idsku = idProductos.map(item => { return item.product_id; });

            var ArraySku = await Producto.find({ id: { $in: idsku } }, { sku: 1, _id: 0 });
            if (ArraySku != null && ArraySku.length != 0) {
                sku = ArraySku.map(item => { return item.sku; });

            }
        }
    }
    return sku;
}

async function obtieneSKUconCat(cat, skuAll) {

    let sku = [];
    var idCategoria = await CategoriaCms.findOne({ slug: cat }, { id: 1, _id: 0 });


    if (idCategoria != null && idCategoria.length != 0) {

        var ids = await Producto.find({ sku: { $in: skuAll } }, { id: 1, _id: 0 });

        var idsArray = ids.map(item => { return item.id; });

        console.log(idCategoria.id);
        var idProductos = await CategoriaProducto.find({ category_id: idCategoria.id, product_id: { $in: idsArray } }, { product_id: 1, _id: 0 });


        if (idProductos != null && idProductos.length != 0) {

            var idsku = idProductos.map(item => { return item.product_id; });

            var ArraySku = await Producto.find({ id: { $in: idsku } }, { sku: 1, _id: 0 });
            if (ArraySku != null && ArraySku.length != 0) {
                sku = ArraySku.map(item => { return item.sku; });

            }
        }
    }

    return sku;
}



module.exports = { establecerPrecio, buscarProductosGenericos, buscarPorSku, prueba, buscarPorCategoria };
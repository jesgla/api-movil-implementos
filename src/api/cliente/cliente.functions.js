const Cliente = require("../../models/clienteModel");
const webAPI = require('../servicios/servicios');
const moment = require("moment");


async function listarCliente(req, res) {
    let body = req.query;

    let cliente = await Cliente.findOne({ "rut": body.rut }, { recomendacionClienteObject: 0 });

    if (cliente != null) {
        return respuesta = {
            error: false,
            msg: "",
            data: cliente
        }
    } else {
        return respuesta = {
            error: false,
            msg: "",
            data: cliente
        }
    }
}

async function bloqueoCliente(req, res) {

    console.log(2);
    let body = req.query;
    let data = await webAPI.getBloqueo(body.rut);

    if (data != "") {
        return data

    } else {
        return "0";
    }
}

async function saldoCliente(req, res) {
    let body = req.query;
    let data = await webAPI.getSaldo(body.rut);
    if (data != null) {
        return data;
    } else {
        return 0;
    }
}

async function facturasCliente(req, res) {
    let body = req.query;
    let data = await webAPI.getFacturas(body.rut);
    if (data != null) {
        return data;
    } else {
        return 0;
    }
}

async function ventasCliente(req, res) {
    let body = req.query;
    let data = await webAPI.getVentas(body.rut, body.cantidad);
    if (data != null) {
        return data;
    } else {
        return 0;
    }
}


async function contactoCliente(req, res) {
    let body = req.body;

    let contacto = {
        funcion: body.funcion,
        rut: body.rut,
        nombre: body.nombre,
        apellido: body.apellido,
        mail: body.mail,
        telefono: body.telefono
    }

    let data = await webAPI.setContacto(contacto);
    if (data != null) {
        return data;
    } else {
        return 0;
    }
}


async function direccionCliente(req, res) {
    let body = req.body;

    let direccion = {
        rut: body.rut,
        tipo: body.tipo,
        region: body.region,
        comuna: body.comuna,
        numero: body.numero,
        provincia: body.provincia,
        direccion: body.direccion,
        localidad: body.localidad,
        latitud: body.latitud,
        longitud: body.longitud,
        codPostal: body.codPostal
    }
    let data = await webAPI.setDireccion(direccion);
    if (data != null) {
        return data;
    } else {
        return 0;
    }
}

obtenerListadoClientes = async ()=>{
    let data = await webAPI.obtenerClientes();
    if (data != null) {
        return data;
    } else {
        return 0;
    }
}

formaPagoCli = async (req)=>{
    
    let body = req.body;
    let rutCliente = body.rutCliente;
    let data = await webAPI.formaPagoCliente(rutCliente);
    if (data != null) {
        return data;
    } else {
        return 0;
    }
}

eventoCliente = async (req)=>{
    let parametros = req.body;
    let data = await webAPI.eventoCli(parametros);
    if (data != null) {
        return data;
    } else {
        return 0;
    }
}

CrearVisita = async (req)=>{
   
    let parametros = req.body;
    
    let data = await webAPI.checkInCliente(parametros);
   
    if (data != null) {
        return data;
    } else {
        return 0;
    }
}

module.exports = { listarCliente, bloqueoCliente, saldoCliente, ventasCliente, facturasCliente, contactoCliente, direccionCliente, obtenerListadoClientes,formaPagoCli,eventoCliente,CrearVisita };
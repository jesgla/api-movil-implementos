const Carro = require("../../models/clienteModel");
const express = require("express");
const { 
    listarCliente, 
    bloqueoCliente, 
    saldoCliente, 
    ventasCliente, 
    facturasCliente, 
    contactoCliente, 
    direccionCliente, 
    obtenerListadoClientes,
    formaPagoCli,
    eventoCliente,
    CrearVisita } = require('../cliente/cliente.functions');
const app = express();

/**
 *Funcion que permite obtener listado de cliente por rut del vendedor
 *
 * @param {*} req rut cliente 
 * @param {*} res respuesta de la consulta 
 * @example /api/cliente/listar?rut=78057000-8
 */
async function ListarCliente(req, res) {
    try {
        let respuesta = await listarCliente(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};

async function BloqueoCliente(req, res) {
    try {
        console.log('1');
        let respuesta = await bloqueoCliente(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};


async function SaldoCliente(req, res) {
    try {
        let respuesta = await saldoCliente(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
}; 


async function FacturasCliente(req, res) {
    try {
        let respuesta = await facturasCliente(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};


async function VentasCliente(req, res) {
    try {
        let respuesta = await ventasCliente(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};

async function ContactoCliente(req, res) {
    try {
        let respuesta = await contactoCliente(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};

/**
 *
 *
 * @param {*} req
 * @param {*} res
 */
async function DireccionCliente(req, res) {
    try {
        let respuesta = await direccionCliente(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};

/**
 * Permite obtener listado de cliente por el rut del vendedor
 * @param {*} req rut vendedor
 * @param {*} res Listado de cliente
 * @example /api/cliente/obtenerClientes?rutVendedor=15738975-0
 */
ObtenerClientes= async (req,res)=>{
    try {
        let respuesta = await obtenerListadoClientes()
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
}

FormaPagoCliente = async (req,res)=>{
    try {
        let respuesta = await formaPagoCli(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
}
CrearEvento = async (req,res)=>{
    try {
        let respuesta = await eventoCliente(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
}

RegistrarVisita = async (req,res)=>{
    try {
        let respuesta = await CrearVisita(req)
        res.send(respuesta);
    } catch (e) {
    
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
}

app.get("/api/cliente/listar", ListarCliente);
app.get("/api/cliente/bloqueo", BloqueoCliente);
app.get("/api/cliente/saldo", SaldoCliente);
app.get("/api/cliente/facturas", FacturasCliente);
app.get("/api/cliente/ventas", VentasCliente);
app.get("/api/cliente/obtenerClientes", ObtenerClientes);

app.post("/api/cliente/formaPago", FormaPagoCliente);
app.post("/api/cliente/crearEvento", CrearEvento);
app.post("/api/cliente/contacto", ContactoCliente);
app.post("/api/cliente/direccion", DireccionCliente);
app.post("/api/cliente/registrarVisita", RegistrarVisita);



module.exports = app;
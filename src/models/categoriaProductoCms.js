const { dbCMS } = require('../config/database');
const Schema = dbCMS.Schema;

const CategoriaProductoSchema = new Schema({

    id: {
        type: Number

    },
    product_id: {
        type: Number
    },
    category_id: {
        type: Number
    }
});
module.exports = dbCMS.model('www_productcategory', CategoriaProductoSchema, 'www_productcategory');
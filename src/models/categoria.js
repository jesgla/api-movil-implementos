const { dbImplenet } = require('../config/database');
const Schema = dbImplenet.Schema;

const CategoriaSchema = new Schema({

    uen: {
        type: String,
        required: false
    },
    categorias: {
        type: Schema.Types.Mixed
    }


}, { timestamps: true });
module.exports = dbImplenet.model('categoria', CategoriaSchema);
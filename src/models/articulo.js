const { dbImplenet } = require('../config/database');

const Schema = dbImplenet.Schema;

const ArticuloSchema = new Schema({
    sku: String,
    atributos: {
        type: Schema.Types.Mixed
    },
    categoria: String,
    cod_id_proveedor: String,
    costo_cero: Number,
    costo_financiero: Number,
    estado: String,
    fabricante: String,
    id_categoria: Number,
    id_linea: Number,
    id_marca: String,
    linea: String,
    marca: String,
    nombre: String,
    numero_parte: String,
    p_minimo: String,
    product: Number,
    uen: String,
    unit_id: String,
    fullText: String,
    image_recid: Number,
    descripcion: String,
    matriz: String,
    sku_matriz: String,
    precios: {
        type: Schema.Types.Mixed
    },
    stockorden : Number
}, { timestamps: true });

module.exports = dbImplenet.model('articulos', ArticuloSchema);
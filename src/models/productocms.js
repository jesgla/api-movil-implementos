const { dbCMS } = require('../config/database');
const Schema = dbCMS.Schema;

const ProductoSchema = new Schema({

    id: {
        type: Number

    },
    sku: {
        type: String
    }
});
module.exports = dbCMS.model('www_product', ProductoSchema, 'www_product');
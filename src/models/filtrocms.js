const { dbCMS } = require('../config/database');
const Schema = dbCMS.Schema;

const FiltroSchema = new Schema({

    id: {
        type: Number

    },
    name: {
        type: String
    },
    values: {
        type: String
    }
});
module.exports = dbCMS.model('www_filter', FiltroSchema, 'www_filter');
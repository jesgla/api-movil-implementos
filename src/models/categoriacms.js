const { dbCMS } = require('../config/database');
const Schema = dbCMS.Schema;

const CategoriaCMSProductoSchema = new Schema({

    id: {
        type: Number
    },
    name: {
        type: String
    },
    slug: {
        type: String
    },
    url: {
        type: String
    },
    image: {
        type: String
    },
    parent_id: {
        type: Number
    },
    on_header: {
        type: Boolean
    }

});
module.exports = dbCMS.model('www_category', CategoriaCMSProductoSchema, 'www_category');
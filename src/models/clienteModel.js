const mongoose = require('mongoose');
//mongoose.set('debug', true);
const Schema = mongoose.Schema;

const ClienteSchema = new Schema({

    rut: {
        type: String
    },
    apellido: {
        type: String
    },
    correos: {
        type: Schema.Types.Mixed
    },
    nombre: {
        type: String
    },
    telefonos: {
        type: Schema.Types.Mixed
    },
    direcciones: {
        type: Schema.Types.Mixed
    },
    ciudad: {
        type: String
    },
    cod_region: {
        type: Date
    },
    provincia: {
        type: String
    },
    clasificacionFinanciera: {
        type: String
    },
    tipoCartera: {
        type: String
    },
    clasificacion: {
        type: String
    },
    condicionPago: {
        type: String
    },
    credito: {
        type: Number
    },
    creditoUtilizado: {
        type: Number
    },
    formaPago: {
        type: String
    },
    giros: {
        type: Schema.Types.Mixed
    },
    segmento: {
        type: String
    },
    subSegmento: {
        type: String
    },
    vendedores: {
        type: Array
    },
    clasificacionCLPot: {
        type: String
    },
    estado: {
        type: String
    },
    contactos: {
        type: Schema.Types.Mixed
    },
    vendedor_cliente: {
        type: Array
    },
    documento_cobros: {
        type: Array
    },
    createdAt: {
        type: Date
    },
    updatedAt: {
        type: Date
    },
    tipo_cliente: {
        type: Number
    },
    recid: {
        type: Number
    }
    /* ,
    recomendacionClienteObject: {
        type: Schema.Types.Mixed
    } */
}, { versionKey: false });

module.exports = mongoose.model('clientes', ClienteSchema, 'clientes');
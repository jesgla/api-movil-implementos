const express = require('express');
const app = express();

app.use(require('./api/catalogo/catalogo.routes'));
app.use(require('./api/categorias/categorias.routes'));
app.use(require('./api/cliente/cliente.routes'));

module.exports = app;